//
//  ViewController.swift
//  JJ_Words
//
//  Created by Japahar Jose on 08/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

import UIKit
import GoogleMobileVision



class ViewController: UIViewController {
    var textDetector:GMVDetector = GMVDetector()

    @IBOutlet weak var signatureView: YPDrawSignatureView!
    @IBOutlet weak var textDisplayLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textDetector = GMVDetector.init(ofType: GMVDetectorTypeText, options: nil)!
        
        signatureView.delegate = self

        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func didClickTextClear(_ sender: Any) {
        signatureView.clear()
        self.textDisplayLbl.text = ""

    }
    
    
    func converToText(){
        DispatchQueue.main.async {
            if let image = self.signatureView.getSignature() {
                let features = self.textDetector.features(in: image.fixOrientation()!.noir!, options: nil) as? [GMVTextBlockFeature]
                // Iterate over each text block.
                for textBlock in features! {
                    print("Text Detected: \(String(describing: textBlock.value))")
                    self.textDisplayLbl.text = textBlock.value
                 
                }
            }

        }
    }

    @IBAction func didClickConvertToText(_ sender: Any) {
        
    }
    
}

extension ViewController:YPSignatureDelegate{
    func didFinish(_ view: YPDrawSignatureView) {
       converToText()
    }
    func didStart(_ view: YPDrawSignatureView) {
        
    }
}
extension UIImage {
    var noir: UIImage? {
        let context = CIContext(options: nil)
        guard let currentFilter = CIFilter(name: "CIPhotoEffectNoir") else { return nil }
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        if let output = currentFilter.outputImage,
            let cgImage = context.createCGImage(output, from: output.extent) {
            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        }
        return nil
    }
}
